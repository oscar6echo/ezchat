# ezchat

## 1 - Summary

**ezchat** stands for easy chat.  
+ No frills chat app.  
+ Hub: [Flask](http://flask.pocoo.org/docs/0.12/), [Flask-socketIO](http://flask-socketio.readthedocs.io/en/latest/) backend.
+ Clients: [VueJS](https://vuejs.org/) frontend.
+ Instant setup.  
+ Available on [PyPI](https://pypi.org/), distributed via [pip](https://pip.pypa.io/en/stable/).

## 2 - Install

From command line:
````
pip install ezchat
````


## 3 - Notebook
See [demo_notebook](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/ezchat/raw/master/demo_ezchat.ipynb).  
In the [notebook](http://jupyter.org/), or [IPython console](https://ipython.org/), the hub and client servers are launched in `detached=True` mode to create new terminal windows and thus avoid blocking the notebook.  

## 4 - Command Line

But in command line they can be launched in `detached=False` after manually creating the terminal windows to 

+ Launch Hub and get back to prompt:

```Python
from ezchat.servers import hub

h = hub(port=5001, detached=True)
````
+ Launch Hub in current terminal:

```Python
from ezchat.servers import hub

hub(port=5001, detached=False)
````
+ Launch Client and get back to prompt:

```Python
from ezchat.servers import client

c = client(port=5002, detached=True)
````
+ Launch Client in current terminal:

```Python
from ezchat.servers import client

client(port=5002, detached=False)
````

+ Kill Hub and Clients - if handles availables:

```Python
from ezchat.servers import kill

kill(h)
kill(c)
````

<!-- pandoc --from=markdown --to=rst --output=README.rst README.md -->
