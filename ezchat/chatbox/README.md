# Chatbox

Single page webapp with framework [vuejs](https://vuejs.org/) and modules:
+ [vuex](https://github.com/vuejs/vuex)
+ [socketio](https://socket.io/)


## User Guide

``` bash
# install node modules
npm install

# webpack dev server with hot reload at localhost:9001
npm run dev

# build bundle for production with minification and gzip
npm run build
```

See [webpack doc](https://webpack.js.org/).

The [demo_notebook](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/ezchat/raw/master/demo_ezchat.ipynb) will serve the built client, located in folder `chatbox/dist`.

